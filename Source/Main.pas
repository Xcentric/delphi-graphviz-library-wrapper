unit Main;

interface

  procedure MainProc;

implementation

uses
  SysUtils, Math,
  Graphviz, Graphviz.Wrapper.Nodes, Graphviz.Wrapper.Edges, Graphviz.Wrapper.Attributes,
  Graphviz.Wrapper.Types, Graphviz.Wrapper.Consts;

procedure MainProc;
  const
    MAX_NODES = 100; MIN_NODES = 2;
    MAX_POS = 3;
    MAX_SIZE = 2; MIN_SIZE = 1;
    MAX_EDGES = MAX_NODES div 2; MIN_EDGES = 1;
    MAX_EDGES_PER_NODE = 6; MIN_EDGES_PER_NODE = 1;
  var
    GV : TGraphviz;
    i, k : Integer;
    LE : TGVLayoutEngine;
    N : TGVNode;
    E : TGVEdge;
    A : TGVAttribute;
begin
  Randomize;
  GV := TGraphviz.Create;
  try
    for i := 1 to RandomRange(MIN_NODES, MAX_NODES) do
      GV.Nodes.Add(
        TGVNode.Create(
          'Node' + IntToStr(i),
          TGVRect.Create(
            TGVPoint.Create(Random * Random(MAX_POS), Random * Random(MAX_POS)),
            Random * RandomRange(MIN_SIZE, MAX_SIZE),
            Random * RandomRange(MIN_SIZE, MAX_SIZE)
          )
        )
      );

    for i := 1 to RandomRange(MIN_EDGES, MAX_EDGES) do
    begin
      N := GV.Nodes[RandomRange(0, GV.Nodes.Count - 1)];
      for k := 1 to RandomRange(MIN_EDGES_PER_NODE, MAX_EDGES_PER_NODE) do
        GV.Edges.Add(
          TGVEdge.Create(
            Format('Edge %d-%d', [i, k]),
            N,
            GV.Nodes[RandomRange(0, GV.Nodes.Count - 1)]
          )
        );
    end;

    for LE := Low(TGVLayoutEngine) to High(TGVLayoutEngine) do
      GV.LayoutGraph(LE, 'D:\Graphviz_' + LayoutEngineNames[LE], [rfDOT, rfPNG]);

    for N in GV.Nodes do
      for A in N.Attributes do
        Writeln(Format('Node "%s" -> %s = %s', [N.Name, A.Name, A.Value]));

    for E in GV.Edges do
      for A in E.Attributes do
        Writeln(Format('Edge "%s" -> %s = %s', [E.Name, A.Name, A.Value]));
  finally
    GV.Free;
  end;
end;

end.
