unit Graphviz.Wrapper.Exceptions;

interface

uses
  SysUtils;

type

  EGraphvizException = class abstract (Exception);

  (* LIBRARY ERRORS *)

  { Common }

  EGVError = class abstract (EGraphvizException)
    private
      FErrorCode : Integer;
    protected
      function DefaultErrMsg : String; virtual; abstract;
      function FormatErrMsg(const ErrMsg : String; ErrCode : Integer) : String; virtual;
      function GetErrDesc(ErrCode : Integer) : String; virtual;
    public
      constructor Create(ErrCode : Integer); overload;
      constructor Create(ErrCode : Integer; const Msg : String); overload;
      constructor CreateFmt(ErrCode : Integer; const Msg : String; const Args : array of const);

      property ErrorCode : Integer read FErrorCode;
  end;

  EGVErrorClass = class of EGVError;

  EGVUnknownError = class (EGVError)
    protected
      function DefaultErrMsg : String; override;
  end;

  { Resource freeing }

  EGVResFreeingError = class abstract (EGVError);

  EGVCloseGraphError = class (EGVResFreeingError)
    protected
      function DefaultErrMsg : String; override;
  end;

  EGVFreeContextError = class (EGVResFreeingError)
    protected
      function DefaultErrMsg : String; override;
  end;

  EGVFreeLayoutError = class (EGVResFreeingError)
    protected
      function DefaultErrMsg : String; override;
  end;

  { Layout }

  EGVCreateLayoutError = class (EGVError)
    protected
      function DefaultErrMsg : String; override;
  end;

  { Rendering }

  EGVRenderError = class abstract (EGVError);

  EGVRenderGraphError = class (EGVRenderError)
    protected
      function DefaultErrMsg : String; override;
  end;

  EGVRenderToFileError = class (EGVRenderError)
    protected
      function DefaultErrMsg : String; override;
  end;

  { Attributes }

  EGVSetAttribError = class (EGVError)
    protected
      function DefaultErrMsg : String; override;
  end;

  { Arguments }

  EGVParseArgsError = class (EGVError)
    protected
      function DefaultErrMsg : String; override;
  end;

  (* WRAPPER EXCEPTIONS *)

  EGVWrapperException = class abstract (EGraphvizException);

  EGVInvalidObjName = class (EGVWrapperException)
    public
      constructor Create(const ObjName : String);
  end;

  EGVInvalidAttrName = class (EGVWrapperException)
    public
      constructor Create(const AttrName : String);
  end;

  EGVDuplicateAttribute = class (EGVWrapperException)
    public
      constructor Create(const AttrName : String);
  end;

  EGVDuplicateObject = class (EGVWrapperException)
    public
      constructor Create(const ObjTypeName, ObjName : String);
  end;

  (* ROUTINES *)

  procedure CheckGVResult(RetVal : Integer; const GVErrorClass : EGVErrorClass); inline;

implementation

uses
  Graphviz.Wrapper.Consts;

procedure CheckGVResult(RetVal : Integer; const GVErrorClass : EGVErrorClass);
begin
  if RetVal <> S_OK then
    if Assigned(GVErrorClass) then
      raise GVErrorClass.Create(RetVal)
    else
      raise EGVUnknownError.Create(RetVal);
end;

{ EGVError }

constructor EGVError.Create(ErrCode : Integer);
begin
  Create(ErrCode, DefaultErrMsg);
end;

constructor EGVError.Create(ErrCode : Integer; const Msg : String);
begin
  inherited Create(FormatErrMsg(Msg, ErrCode));

  FErrorCode := ErrCode;
end;

constructor EGVError.CreateFmt(ErrCode : Integer; const Msg : String; const Args : array of const);
begin
  inherited CreateFmt(FormatErrMsg(Msg, ErrCode), Args);

  FErrorCode := ErrCode;
end;

function EGVError.FormatErrMsg(const ErrMsg : String; ErrCode : Integer) : String;
begin
  Result := Format('%s%s%s%s(%d)', [ErrMsg, sLineBreak, GetErrDesc(ErrCode), sLineBreak, ErrCode]);
end;

function EGVError.GetErrDesc(ErrCode : Integer) : String;
begin
  Result := SysErrorMessage(ErrCode);
end;

{ EGVUnknownError }

function EGVUnknownError.DefaultErrMsg : String;
begin
  Result := GV_ERR_UNKNOWN;
end;

{ EGVCloseGraphError }

function EGVCloseGraphError.DefaultErrMsg : String;
begin
  Result := GV_ERR_CLOSE_GRAPH;
end;

{ EGVFreeContextError }

function EGVFreeContextError.DefaultErrMsg : String;
begin
  Result := GV_ERR_FREE_CONTEXT;
end;

{ EGVFreeLayoutError }

function EGVFreeLayoutError.DefaultErrMsg : String;
begin
  Result := GV_ERR_FREE_LAYOUT;
end;

{ EGVCreateLayoutError }

function EGVCreateLayoutError.DefaultErrMsg : String;
begin
  Result := GV_ERR_CREATE_LAYOUT;
end;

{ EGVRenderGraphError }

function EGVRenderGraphError.DefaultErrMsg : String;
begin
  Result := GV_ERR_RENDER_GRAPH;
end;

{ EGVRenderToFileError }

function EGVRenderToFileError.DefaultErrMsg : String;
begin
  Result := GV_ERR_RENDER_TO_FILE;
end;

{ EGVSetAttribError }

function EGVSetAttribError.DefaultErrMsg : String;
begin
  Result := GV_ERR_SET_ATTRIBUTE;
end;

{ EGVParseArgsError }

function EGVParseArgsError.DefaultErrMsg : String;
begin
  Result := GV_ERR_PARSE_ARGS;
end;

{ EGVDuplicateAttribute }

constructor EGVDuplicateAttribute.Create(const AttrName : String);
begin
  inherited CreateFmt(GV_ERR_DUP_ATTRIBUTE, [AttrName]);
end;

{ EGVInvalidObjName }

constructor EGVInvalidObjName.Create(const ObjName : String);
begin
  inherited CreateFmt(GV_ERR_INVALID_OBJ_NAME, [ObjName]);
end;

{ EGVInvalidAttrName }

constructor EGVInvalidAttrName.Create(const AttrName : String);
begin
  inherited CreateFmt(GV_ERR_INVALID_ATTR_NAME, [AttrName]);
end;

{ EGVDuplicateObject }

constructor EGVDuplicateObject.Create(const ObjTypeName, ObjName : String);
begin
  inherited CreateFmt(GV_ERR_DUP_OBJECT, [ObjTypeName, ObjName]);
end;

end.
