unit Graphviz.Wrapper.API;

interface

uses
  Graphviz.Import.Types, Graphviz.Import.Structures,
  Graphviz.Wrapper.Types, Graphviz.Wrapper.Consts, Graphviz.Wrapper.Exceptions;

type

  TGraphvizAPI = class abstract
    strict protected
      class procedure CloseGraph(Graph : PGVGraph); inline;
      class function  CreateContext : PGVContext; inline;
      class function  FirstEdge(Graph : PGVGraph; OrgNode : PGVNode) : PGVEdge; inline;
      class function  FirstNode(Graph : PGVGraph) : PGVNode; inline;
      class procedure FreeContext(GVC : PGVContext); inline;
      class procedure FreeLayout(GVC : PGVContext; Graph : PGVGraph); inline;
      class function  GetAttribValue(Obj : PGVPointer; const AttribName : TGVString) : TGVString; inline;
      class function  GetEdge(Graph : PGVGraph; NodeA, NodeB : PGVNode; const EdgeName : TGVString;
        CreateFlag : TGVBool) : PGVEdge; inline;
      class function  GetNode(Graph : PGVGraph; const NodeName : TGVString;
        CreateFlag : TGVBool) : PGVNode; inline;
      class function  NextEdge(Graph : PGVGraph; PrevEdge : PGVEdge; OrgNode : PGVNode) : PGVEdge; inline;
      class function  NextNode(Graph : PGVGraph; PrevNode : PGVNode) : PGVNode; inline;
      class function  OpenGraph(const GraphName : TGVString; GraphType : PGVGraphDesc) : PGVGraph; inline;
      class procedure ParseArgs(GVC : PGVContext; ArgsCount : Integer; ArgsArray : PGVStringArray); inline; experimental;
      class procedure PerformLayout(GVC : PGVContext; Graph : PGVGraph; const EngineName : TGVString); inline;
      class procedure RenderGraph(GVC : PGVContext; Graph : PGVGraph; const EngineName : TGVString); inline;
      class procedure RenderGraphToFile(GVC : PGVContext; Graph : PGVGraph;
        const FileFormat, FileName : TGVString); inline;
      class procedure SetAttribValue(Obj : PGVPointer; const AttribName, AttribValue : TGVString;
        const AttribDefValue : TGVString = GV_EMPTY_STR); inline;
  end;

implementation

uses
  Graphviz.Import.Intf;

{ TGraphvizAPI }

class procedure TGraphvizAPI.CloseGraph(Graph : PGVGraph);
begin
  CheckGVResult(agclose(Graph), EGVCloseGraphError);
end;

class function TGraphvizAPI.CreateContext : PGVContext;
begin
  Result := gvContext;
end;

class function TGraphvizAPI.FirstEdge(Graph : PGVGraph; OrgNode : PGVNode) : PGVEdge;
begin
  Result := agfstedge(Graph, OrgNode);
end;

class function TGraphvizAPI.FirstNode(Graph : PGVGraph) : PGVNode;
begin
  Result := agfstnode(Graph);
end;

class procedure TGraphvizAPI.FreeContext(GVC : PGVContext);
begin
  CheckGVResult(gvFreeContext(GVC), EGVFreeContextError);
end;

class procedure TGraphvizAPI.FreeLayout(GVC : PGVContext; Graph : PGVGraph);
begin
  CheckGVResult(gvFreeLayout(GVC, Graph), EGVFreeLayoutError);
end;

class function TGraphvizAPI.GetAttribValue(Obj : PGVPointer; const AttribName : TGVString) : TGVString;
begin
  Result := agget(Obj, PGVString(AttribName));
end;

class function TGraphvizAPI.GetEdge(Graph : PGVGraph; NodeA, NodeB : PGVNode; const EdgeName : TGVString;
  CreateFlag : TGVBool) : PGVEdge;
begin
  Result := agedge(Graph, NodeA, NodeB, PGVString(EdgeName), CreateFlag);
end;

class function TGraphvizAPI.GetNode(Graph : PGVGraph; const NodeName : TGVString; CreateFlag : TGVBool) : PGVNode;
begin
  Result := agnode(Graph, PGVString(NodeName), CreateFlag);
end;

class function TGraphvizAPI.NextEdge(Graph : PGVGraph; PrevEdge : PGVEdge; OrgNode : PGVNode) : PGVEdge;
begin
  Result := agnxtedge(Graph, PrevEdge, OrgNode);
end;

class function TGraphvizAPI.NextNode(Graph : PGVGraph; PrevNode : PGVNode) : PGVNode;
begin
  Result := agnxtnode(Graph, PrevNode);
end;

class function TGraphvizAPI.OpenGraph(const GraphName : TGVString; GraphType : PGVGraphDesc) : PGVGraph;
begin
  Result := agopen(PGVString(GraphName), GraphType);
end;

class procedure TGraphvizAPI.ParseArgs(GVC : PGVContext; ArgsCount : Integer; ArgsArray : PGVStringArray);
begin
  CheckGVResult(gvParseArgs(GVC, ArgsCount, ArgsArray), EGVParseArgsError);
end;

class procedure TGraphvizAPI.PerformLayout(GVC : PGVContext; Graph : PGVGraph; const EngineName : TGVString);
begin
  CheckGVResult(gvLayout(GVC, Graph, PGVString(EngineName)), EGVCreateLayoutError);
end;

class procedure TGraphvizAPI.RenderGraph(GVC : PGVContext; Graph : PGVGraph; const EngineName : TGVString);
begin
  CheckGVResult(gvRender(GVC, Graph, PGVString(EngineName)), EGVRenderGraphError);
end;

class procedure TGraphvizAPI.RenderGraphToFile(GVC : PGVContext; Graph : PGVGraph;
  const FileFormat, FileName : TGVString);
begin
  CheckGVResult(gvRenderFilename(GVC, Graph, PGVString(FileFormat), PGVString(FileName)), EGVRenderToFileError);
end;

class procedure TGraphvizAPI.SetAttribValue(Obj : PGVPointer; const AttribName, AttribValue, AttribDefValue : TGVString);
  var
    AttrDefVal : TGVString;
begin
  if AttribDefValue = GV_EMPTY_STR then
    AttrDefVal := AttribValue
  else
    AttrDefVal := AttribDefValue;

  CheckGVResult(agsafeset(Obj, PGVString(AttribName), PGVString(AttribValue), PGVString(AttrDefVal)), EGVSetAttribError);
end;

end.
