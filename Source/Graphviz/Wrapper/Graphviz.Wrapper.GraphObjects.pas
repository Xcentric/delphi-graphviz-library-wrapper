unit Graphviz.Wrapper.GraphObjects;

interface

uses
  Classes, Contnrs,
  Graphviz.Wrapper.Attributes, Graphviz.Wrapper.Types;

type

  TGVObject = class abstract
    private
      FAttributes : TGVAttrList;
      FName : TGVString;
    protected
      class function GetObjectTypeName : String; virtual; abstract;

      procedure ValidateName(const AName : TGVString); virtual;
    public
      constructor Create(const AName : TGVString);
      destructor Destroy; override;

      procedure PutDimensionsToAttributes; virtual; abstract;
      procedure SetDimensionsFromAttributes; virtual; abstract;

      property Attributes : TGVAttrList read FAttributes;
      property Name : TGVString read FName;
  end;

  TGVObjectListEnumerator = class (TListEnumerator)
    public
      function GetCurrent : TGVObject;

      property Current : TGVObject read GetCurrent;
  end;

  TGVObjectList = class (TObjectList)
    private
      procedure CheckUnique(AObject : TGVObject);
    protected
      function  GetItem(Index : Integer) : TGVObject;
      procedure SetItem(Index : Integer; AObject : TGVObject);
    public
      function  Add(AObject : TGVObject) : Integer;
      function  AddNonExistent(AObject : TGVObject) : Integer;
      function  Contains(AObject : TGVObject) : Boolean; overload;
      function  Contains(const ObjName : TGVString) : Boolean; overload;
      function  Extract(Item : TGVObject) : TGVObject;
      function  Find(const ObjName : TGVString) : TGVObject;
      function  First : TGVObject;
      function  GetEnumerator : TGVObjectListEnumerator;
      function  IndexOf(AObject : TGVObject) : Integer;
      procedure Insert(Index : Integer; AObject : TGVObject);
      function  Last : TGVObject;
      function  Remove(AObject : TGVObject) : Integer;

      property Items[Index : Integer] : TGVObject read GetItem write SetItem; default;
  end;

implementation

uses
  Graphviz.Wrapper.Utils, Graphviz.Wrapper.Exceptions;

{ TGVObject }

constructor TGVObject.Create(const AName : TGVString);
begin
  inherited Create;

  ValidateName(AName);
  FName := AName;
  FAttributes := TGVAttrList.Create;
end;

destructor TGVObject.Destroy;
begin
  FAttributes.Free;

  inherited Destroy;
end;

procedure TGVObject.ValidateName(const AName : TGVString);
begin
  if not IsValidGVString(AName) then
    raise EGVInvalidObjName.Create(AName);
end;

{ TGVObjectListEnumerator }

function TGVObjectListEnumerator.GetCurrent : TGVObject;
begin
  Result := TGVObject(inherited GetCurrent);
end;

{ TGVObjectList }

function TGVObjectList.Add(AObject : TGVObject) : Integer;
begin
  CheckUnique(AObject);

  Result := inherited Add(AObject);
end;

function TGVObjectList.AddNonExistent(AObject : TGVObject) : Integer;
begin
  Result := -1;
  if not Contains(AObject.Name) then
    Result := Add(AObject);
end;

procedure TGVObjectList.CheckUnique(AObject : TGVObject);
begin
  if Assigned(Find(AObject.Name)) then
    raise EGVDuplicateObject.Create(AObject.GetObjectTypeName, AObject.Name);
end;

function TGVObjectList.Contains(const ObjName : TGVString) : Boolean;
begin
  Result := Assigned(Find(ObjName));
end;

function TGVObjectList.Contains(AObject : TGVObject) : Boolean;
begin
  Result := IndexOf(AObject) >= 0;
end;

function TGVObjectList.Extract(Item : TGVObject) : TGVObject;
begin
  Result := inherited Extract(Item) as TGVObject;
end;

function TGVObjectList.Find(const ObjName : TGVString) : TGVObject;
  var
    Obj : TGVObject;
begin
  Result := nil;
  for Obj in Self do
    if Obj.Name = ObjName then
    begin
      Result := Obj;
      Break;
    end;
end;

function TGVObjectList.First : TGVObject;
begin
  Result := inherited First as TGVObject;
end;

function TGVObjectList.GetEnumerator : TGVObjectListEnumerator;
begin
  Result := TGVObjectListEnumerator.Create(Self);
end;

function TGVObjectList.GetItem(Index : Integer) : TGVObject;
begin
  Result := inherited GetItem(Index) as TGVObject;
end;

function TGVObjectList.IndexOf(AObject : TGVObject) : Integer;
begin
  Result := inherited IndexOf(AObject);
end;

procedure TGVObjectList.Insert(Index : Integer; AObject : TGVObject);
begin
  CheckUnique(AObject);

  inherited Insert(Index, AObject);
end;

function TGVObjectList.Last : TGVObject;
begin
  Result := inherited Last as TGVObject;
end;

function TGVObjectList.Remove(AObject : TGVObject) : Integer;
begin
  Result := inherited Remove(AObject);
end;

procedure TGVObjectList.SetItem(Index : Integer; AObject : TGVObject);
begin
  CheckUnique(AObject);

  inherited SetItem(Index, AObject);
end;

end.
