unit Graphviz.Wrapper.Utils;

interface

uses
  Graphviz.Wrapper.Types;

type

  TGVAttribUtils = record
    public
      class procedure ExtractPoints(const AttrValue : TGVString; out GVPoints : TGVPoints); static;
      class function  FormatCoord(GVCoord : TGVCoord) : TGVString; static;
      class function  FormatPoint(GVPoint : TGVPoint) : TGVString; static;
      class function  FormatPoints(const GVPoints : TGVPoints) : TGVString; static;
      class function  GetCoord(const AttrValue : TGVString) : TGVCoord; static;
      class function  GetPoint(const AttrValue : TGVString) : TGVPoint; static;
  end;

  function IsValidGVString(const S : TGVString) : Boolean;

implementation

uses
  SysUtils, Math,
  Graphviz.Wrapper.Consts;

{ Regular routines }

function IsValidGVString(const S : TGVString) : Boolean;
begin
  Result := Length(Trim(S)) > 0;
end;

{ TGVAttribUtils }

class procedure TGVAttribUtils.ExtractPoints(const AttrValue : TGVString; out GVPoints : TGVPoints);
  var
    sTmp : TGVString;
    iCount, iDelimPos : Integer;
begin
  sTmp := AttrValue + CHR_POINTS_DELIMITER;
  iCount := 0;

  repeat
    iDelimPos := Pos(CHR_POINTS_DELIMITER, sTmp);
    if iDelimPos > 0 then
    begin
      Inc(iCount);
      SetLength(GVPoints, iCount);
      GVPoints[High(GVPoints)] := GetPoint(Copy(sTmp, 1, iDelimPos - 1));
      Delete(sTmp, 1, iDelimPos);
    end;
  until iDelimPos <= 0;
end;

class function TGVAttribUtils.FormatCoord(GVCoord : TGVCoord) : TGVString;
begin
  Result := Format('%g', [GVCoord]);
end;

class function TGVAttribUtils.FormatPoint(GVPoint : TGVPoint) : TGVString;
begin
  Result := FormatCoord(GVPoint.X) + CHR_COORDS_DELIMITER + FormatCoord(GVPoint.Y);
end;

class function TGVAttribUtils.FormatPoints(const GVPoints : TGVPoints) : TGVString;
  var
    P : TGVPoint;
begin
  Result := EmptyStr;
  for P in GVPoints do
    if Result = EmptyStr then
      Result := FormatPoint(P)
    else
      Result := Result + CHR_POINTS_DELIMITER + FormatPoint(P);
end;

class function TGVAttribUtils.GetCoord(const AttrValue : TGVString) : TGVCoord;
  var
    GVCoord : TGVCoord;
    iErrPos : Integer;
begin
  Result := NaN;
  Val(AttrValue, GVCoord, iErrPos);
  if iErrPos = 0 then
    Result := GVCoord;
end;

class function TGVAttribUtils.GetPoint(const AttrValue : TGVString) : TGVPoint;
  var
    iDelimPos : Integer;
begin
  Result.X := NaN;
  Result.Y := NaN;

  iDelimPos := Pos(CHR_COORDS_DELIMITER, AttrValue);
  if iDelimPos > 0 then
  begin
    Result.X := GetCoord(Copy(AttrValue, 1, iDelimPos - 1));
    Result.Y := GetCoord(Copy(AttrValue, iDelimPos + 1, Length(AttrValue)));
  end;
end;

end.
