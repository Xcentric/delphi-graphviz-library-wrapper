unit Graphviz.Wrapper.Attributes;

interface

uses
  Classes, Contnrs,
  Graphviz.Wrapper.Types;

type

  TGVAttribute = class
    private
      FName,
      FValue : TGVString;
    protected
      procedure ValidateName(const AName : TGVString);
    public
      constructor Create(const AttrName, AttrValue : TGVString);

      property Name : TGVString read FName;
      property Value : TGVString read FValue write FValue;
  end;

  TGVAttributes = array of TGVAttribute;

  TGVAttrListEnumerator = class (TListEnumerator)
    public
      function GetCurrent : TGVAttribute;

      property Current : TGVAttribute read GetCurrent;
  end;

  TGVAttrList = class (TObjectList)
    private
      procedure CheckUnique(AObject : TGVAttribute);
    protected
      function  GetItem(Index : Integer) : TGVAttribute;
      procedure SetItem(Index : Integer; AObject : TGVAttribute);
    public
      function  Add(AObject : TGVAttribute) : Integer;
      function  Contains(const AttrName : TGVString) : Boolean;
      function  Extract(Item : TGVAttribute) : TGVAttribute;
      function  Find(const AttrName : TGVString) : TGVAttribute;
      function  First : TGVAttribute;
      function  GetEnumerator : TGVAttrListEnumerator;
      function  IndexOf(AObject : TGVAttribute) : Integer;
      procedure Insert(Index : Integer; AObject : TGVAttribute);
      function  Last : TGVAttribute;
      function  Remove(AObject : TGVAttribute) : Integer;
      function  ToArray : TGVAttributes;
      procedure Update(const AttrName, AttrValue : TGVString);

      property Items[Index : Integer] : TGVAttribute read GetItem write SetItem; default;
  end;

implementation

uses
  Graphviz.Wrapper.Utils, Graphviz.Wrapper.Exceptions;

{ TGVAttribute }

constructor TGVAttribute.Create(const AttrName, AttrValue : TGVString);
begin
  inherited Create;

  ValidateName(AttrName);
  FName  := AttrName;
  FValue := AttrValue;
end;

procedure TGVAttribute.ValidateName(const AName : TGVString);
begin
  if not IsValidGVString(AName) then
    raise EGVInvalidAttrName.Create(AName);
end;

{ TGVAttrListEnumerator }

function TGVAttrListEnumerator.GetCurrent : TGVAttribute;
begin
  Result := TGVAttribute(inherited GetCurrent);
end;

{ TGVAttrList }

function TGVAttrList.Add(AObject : TGVAttribute) : Integer;
begin
  CheckUnique(AObject);

  Result := inherited Add(AObject);
end;

procedure TGVAttrList.CheckUnique(AObject : TGVAttribute);
begin
  if Assigned(Find(AObject.Name)) then
    raise EGVDuplicateAttribute.Create(AObject.Name);
end;

function TGVAttrList.Contains(const AttrName : TGVString) : Boolean;
begin
  Result := Assigned(Find(AttrName));
end;

function TGVAttrList.Extract(Item : TGVAttribute) : TGVAttribute;
begin
  Result := inherited Extract(Item) as TGVAttribute;
end;

function TGVAttrList.Find(const AttrName : TGVString) : TGVAttribute;
  var
    Attr : TGVAttribute;
begin
  Result := nil;
  for Attr in Self do
    if Attr.Name = AttrName then
    begin
      Result := Attr;
      Break;
    end;
end;

function TGVAttrList.First : TGVAttribute;
begin
  Result := inherited First as TGVAttribute;
end;

function TGVAttrList.GetEnumerator : TGVAttrListEnumerator;
begin
  Result := TGVAttrListEnumerator.Create(Self);
end;

function TGVAttrList.GetItem(Index : Integer) : TGVAttribute;
begin
  Result := inherited GetItem(Index) as TGVAttribute;
end;

function TGVAttrList.IndexOf(AObject : TGVAttribute) : Integer;
begin
  Result := inherited IndexOf(AObject);
end;

procedure TGVAttrList.Insert(Index : Integer; AObject : TGVAttribute);
begin
  CheckUnique(AObject);

  inherited Insert(Index, AObject);
end;

function TGVAttrList.Last : TGVAttribute;
begin
  Result := inherited Last as TGVAttribute;
end;

function TGVAttrList.Remove(AObject : TGVAttribute) : Integer;
begin
  Result := inherited Remove(AObject);
end;

procedure TGVAttrList.SetItem(Index : Integer; AObject : TGVAttribute);
begin
  CheckUnique(AObject);

  inherited SetItem(Index, AObject);
end;

function TGVAttrList.ToArray : TGVAttributes;
  var
    i : Integer;
begin
  SetLength(Result, Count);
  for i := 0 to Count - 1 do
    Result[i] := Items[i];
end;

procedure TGVAttrList.Update(const AttrName, AttrValue : TGVString);
  var
    Attr : TGVAttribute;
begin
  Attr := Find(AttrName);
  if Assigned(Attr) then
    Attr.Value := AttrValue
  else
    Add(TGVAttribute.Create(AttrName, AttrValue));
end;

end.
