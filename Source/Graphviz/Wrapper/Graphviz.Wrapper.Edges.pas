unit Graphviz.Wrapper.Edges;

interface

uses
  Classes, 
  Graphviz.Wrapper.GraphObjects, Graphviz.Wrapper.Nodes, Graphviz.Wrapper.Points, Graphviz.Wrapper.Types;

type

  TGVEdge = class (TGVObject)
    private
      FNodeA,
      FNodeB : TGVNode;
      FPoints : TGVPointObjList;
    protected
      class function GetObjectTypeName : String; override;
    public
      constructor Create(const AName : TGVString; const ANodeA, ANodeB : TGVNode; const EdgePoints : TGVPoints = nil);
      destructor Destroy; override;

      procedure PutDimensionsToAttributes; override;
      procedure SetDimensionsFromAttributes; override;

      property NodeA : TGVNode read FNodeA;
      property NodeB : TGVNode read FNodeB;
      property Points : TGVPointObjList read FPoints;
  end;

  TGVEdgeListEnumerator = class (TListEnumerator)
    public
      function GetCurrent : TGVEdge;

      property Current : TGVEdge read GetCurrent;
  end;

  TGVEdgeList = class (TGVObjectList)
    protected
      function  GetItem(Index : Integer) : TGVEdge;
      procedure SetItem(Index : Integer; AObject : TGVEdge);
    public
      function  Add(AObject : TGVEdge) : Integer;
      function  AddNonExistent(AObject : TGVEdge) : Integer;
      function  Extract(Item : TGVEdge) : TGVEdge;
      function  Find(const EdgeName : TGVString) : TGVEdge;
      function  First : TGVEdge;
      function  GetEnumerator : TGVEdgeListEnumerator;
      function  IndexOf(AObject : TGVEdge) : Integer;
      procedure Insert(Index : Integer; AObject : TGVEdge);
      function  Last : TGVEdge;
      function  Remove(AObject : TGVEdge) : Integer;

      property Items[Index : Integer] : TGVEdge read GetItem write SetItem; default;
  end;

implementation

uses
  Graphviz.Import.Consts,
  Graphviz.Wrapper.Attributes, Graphviz.Wrapper.Utils, Graphviz.Wrapper.Consts;

{ TGVEdge }

constructor TGVEdge.Create(const AName : TGVString; const ANodeA, ANodeB : TGVNode; const EdgePoints : TGVPoints);
begin
  inherited Create(AName);

  FNodeA := ANodeA;
  FNodeB := ANodeB;

  if Length(EdgePoints) = 0 then
    FPoints := TGVPointObjList.Create
  else
  begin
    FPoints := TGVPointObjList.Create(EdgePoints);
    PutDimensionsToAttributes;
  end;
end;

destructor TGVEdge.Destroy;
begin
  FPoints.Free;

  inherited Destroy;
end;

class function TGVEdge.GetObjectTypeName : String;
begin
  Result := GV_EDGE_TYPE_NAME;
end;

procedure TGVEdge.PutDimensionsToAttributes;
begin
  Attributes.Update(GV_ATTR_POS, TGVAttribUtils.FormatPoints(FPoints.ToArray));
end;

procedure TGVEdge.SetDimensionsFromAttributes;
  var
    Pts : TGVPoints;
begin
  TGVAttribUtils.ExtractPoints(Attributes.Find(GV_ATTR_POS).Value, Pts);
  FPoints.FromArray(Pts);
end;

{ TGVEdgeListEnumerator }

function TGVEdgeListEnumerator.GetCurrent : TGVEdge;
begin
  Result := TGVEdge(inherited GetCurrent);
end;

{ TGVEdgeList }

function TGVEdgeList.Add(AObject : TGVEdge) : Integer;
begin
  Result := inherited Add(AObject);
end;

function TGVEdgeList.AddNonExistent(AObject : TGVEdge) : Integer;
begin
  Result := inherited AddNonExistent(AObject);
end;

function TGVEdgeList.Extract(Item : TGVEdge) : TGVEdge;
begin
  Result := inherited Extract(Item) as TGVEdge;
end;

function TGVEdgeList.Find(const EdgeName : TGVString) : TGVEdge;
begin
  Result := inherited Find(EdgeName) as TGVEdge;
end;

function TGVEdgeList.First : TGVEdge;
begin
  Result := inherited First as TGVEdge;
end;

function TGVEdgeList.GetEnumerator : TGVEdgeListEnumerator;
begin
  Result := TGVEdgeListEnumerator.Create(Self);
end;

function TGVEdgeList.GetItem(Index : Integer) : TGVEdge;
begin
  Result := inherited GetItem(Index) as TGVEdge;
end;

function TGVEdgeList.IndexOf(AObject : TGVEdge) : Integer;
begin
  Result := inherited IndexOf(AObject);
end;

procedure TGVEdgeList.Insert(Index : Integer; AObject : TGVEdge);
begin
  inherited Insert(Index, AObject);
end;

function TGVEdgeList.Last : TGVEdge;
begin
  Result := inherited Last as TGVEdge;
end;

function TGVEdgeList.Remove(AObject : TGVEdge) : Integer;
begin
  Result := inherited Remove(AObject);
end;

procedure TGVEdgeList.SetItem(Index : Integer; AObject : TGVEdge);
begin
  inherited SetItem(Index, AObject);
end;

end.
