unit Graphviz.Wrapper.Nodes;

interface

uses
  Classes, 
  Graphviz.Wrapper.GraphObjects, Graphviz.Wrapper.Types;

type

  TGVNode = class (TGVObject)
    private
      FRect : TGVRect;
    protected
      class function GetObjectTypeName : String; override;
    public
      constructor Create(const AName : TGVString; NodeRect : TGVRect);

      procedure PutDimensionsToAttributes; override;
      procedure SetDimensionsFromAttributes; override;

      property Rect : TGVRect read FRect write FRect;
  end;

  TGVNodeListEnumerator = class (TListEnumerator)
    public
      function GetCurrent : TGVNode;

      property Current : TGVNode read GetCurrent;
  end;

  TGVNodeList = class (TGVObjectList)
    protected
      function  GetItem(Index : Integer) : TGVNode;
      procedure SetItem(Index : Integer; AObject : TGVNode);
    public
      function  Add(AObject : TGVNode) : Integer;
      function  AddNonExistent(AObject : TGVNode) : Integer;
      function  Extract(Item : TGVNode) : TGVNode;
      function  Find(const NodeName : TGVString) : TGVNode;
      function  First : TGVNode;
      function  GetEnumerator : TGVNodeListEnumerator;
      function  IndexOf(AObject : TGVNode) : Integer;
      procedure Insert(Index : Integer; AObject : TGVNode);
      function  Last : TGVNode;
      function  Remove(AObject : TGVNode) : Integer;

      property Items[Index : Integer] : TGVNode read GetItem write SetItem; default;
  end;

implementation

uses
  Graphviz.Import.Consts,
  Graphviz.Wrapper.Attributes, Graphviz.Wrapper.Utils, Graphviz.Wrapper.Consts;

{ TGVNode }

constructor TGVNode.Create(const AName : TGVString; NodeRect : TGVRect);
begin
  inherited Create(AName);

  FRect := NodeRect;
  with Attributes do
  begin
    Add(TGVAttribute.Create(GV_ATTR_POS, TGVAttribUtils.FormatPoint(FRect.Center)));
    Add(TGVAttribute.Create(GV_ATTR_WIDTH, TGVAttribUtils.FormatCoord(FRect.Width)));
    Add(TGVAttribute.Create(GV_ATTR_HEIGHT, TGVAttribUtils.FormatCoord(FRect.Height)));
  end;
end;

class function TGVNode.GetObjectTypeName : String;
begin
  Result := GV_NODE_TYPE_NAME;
end;

procedure TGVNode.PutDimensionsToAttributes;
begin
  with Attributes do
  begin
    Update(GV_ATTR_POS, TGVAttribUtils.FormatPoint(FRect.Center));
    Update(GV_ATTR_WIDTH, TGVAttribUtils.FormatCoord(FRect.Width));
    Update(GV_ATTR_HEIGHT, TGVAttribUtils.FormatCoord(FRect.Height));
  end;
end;

procedure TGVNode.SetDimensionsFromAttributes;
begin
  FRect := TGVRect.Create(
    TGVAttribUtils.GetPoint(Attributes.Find(GV_ATTR_POS).Value),
    TGVAttribUtils.GetCoord(Attributes.Find(GV_ATTR_WIDTH).Value),
    TGVAttribUtils.GetCoord(Attributes.Find(GV_ATTR_HEIGHT).Value));
end;

{ TGVNodeListEnumerator }

function TGVNodeListEnumerator.GetCurrent : TGVNode;
begin
  Result := TGVNode(inherited GetCurrent);
end;

{ TGVNodeList }

function TGVNodeList.Add(AObject : TGVNode) : Integer;
begin
  Result := inherited Add(AObject);
end;

function TGVNodeList.AddNonExistent(AObject : TGVNode) : Integer;
begin
  Result := inherited AddNonExistent(AObject);
end;

function TGVNodeList.Extract(Item : TGVNode) : TGVNode;
begin
  Result := inherited Extract(Item) as TGVNode;
end;

function TGVNodeList.Find(const NodeName : TGVString) : TGVNode;
begin
  Result := inherited Find(NodeName) as TGVNode;
end;

function TGVNodeList.First : TGVNode;
begin
  Result := inherited First as TGVNode;
end;

function TGVNodeList.GetEnumerator : TGVNodeListEnumerator;
begin
  Result := TGVNodeListEnumerator.Create(Self);
end;

function TGVNodeList.GetItem(Index : Integer) : TGVNode;
begin
  Result := inherited GetItem(Index) as TGVNode;
end;

function TGVNodeList.IndexOf(AObject : TGVNode) : Integer;
begin
  Result := inherited IndexOf(AObject);
end;

procedure TGVNodeList.Insert(Index : Integer; AObject : TGVNode);
begin
  inherited Insert(Index, AObject);
end;

function TGVNodeList.Last : TGVNode;
begin
  Result := inherited Last as TGVNode;
end;

function TGVNodeList.Remove(AObject : TGVNode) : Integer;
begin
  Result := inherited Remove(AObject);
end;

procedure TGVNodeList.SetItem(Index : Integer; AObject : TGVNode);
begin
  inherited SetItem(Index, AObject);
end;

end.
