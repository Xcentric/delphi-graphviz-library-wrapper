unit Graphviz.Wrapper.Types;

interface

type

  { Simple types }

  TGVString = type AnsiString;
  TGVCoord = type Double;

  { Enums }

  TGVLayoutEngine = (leCirco, leDot, leFdp, leNeato, leOsage, leSfdp, leTwopi);

  TGVRenderFormat = (rfBMP, rfDOT, rfGIF, rfJPEG, rfPDF, rfPlain, rfPNG, rfSVG);

  TGVRenderFormats = set of TGVRenderFormat;

  { Structures }

  TGVPoint = record
    X,
    Y : TGVCoord;

    constructor Create(AX, AY : TGVCoord);
  end;

  TGVPoints = array of TGVPoint;

  TGVRect = record
    Center : TGVPoint;
    Height,
    Width : TGVCoord;
    LeftBottom,
    RightTop : TGVPoint;

    constructor Create(ALeftBottom, ARightTop : TGVPoint); overload;
    constructor Create(ACenter : TGVPoint; AWidth, AHeight : TGVCoord); overload;
  end;

implementation

{ TGVPoint }

constructor TGVPoint.Create(AX, AY : TGVCoord);
begin
  X := AX;
  Y := AY;
end;

{ TGVRect }

constructor TGVRect.Create(ALeftBottom, ARightTop : TGVPoint);
begin
  LeftBottom := ALeftBottom;
  RightTop   := ARightTop;

  Center.X := (RightTop.X - LeftBottom.X) / 2;
  Center.Y := (RightTop.Y - LeftBottom.Y) / 2;
  Width    :=  RightTop.X - LeftBottom.X;
  Height   :=  RightTop.Y - LeftBottom.Y;
end;

constructor TGVRect.Create(ACenter : TGVPoint; AWidth, AHeight : TGVCoord);
begin
  Center := ACenter;
  Width  := AWidth;
  Height := AHeight;

  LeftBottom.X := Center.X - Width  / 2;
  LeftBottom.Y := Center.Y - Height / 2;
  RightTop.X   := Center.X + Width  / 2;
  RightTop.Y   := Center.Y + Height / 2;
end;

end.
