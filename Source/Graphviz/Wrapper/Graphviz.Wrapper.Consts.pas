unit Graphviz.Wrapper.Consts;

interface

uses
  Graphviz.Import.Consts,
  Graphviz.Wrapper.Types;

const

  { Simple }

  CHR_COORDS_DELIMITER = ',';   // do not localize
  CHR_POINTS_DELIMITER = ' ';   // do not localize

  GV_EMPTY_STR = '';

  TWIPS_PER_INCH = 1440.0;

  { Arrays }

  LayoutEngineNames : array [Low(TGVLayoutEngine)..High(TGVLayoutEngine)] of TGVString =
    (GV_ENGINE_CIRCO, GV_ENGINE_DOT, GV_ENGINE_FDP, GV_ENGINE_NEATO,
     GV_ENGINE_OSAGE, GV_ENGINE_SFDP, GV_ENGINE_TWOPI);

  RenderFormatNames : array [Low(TGVRenderFormat)..High(TGVRenderFormat)] of TGVString =
    (GV_RENDER_BMP, GV_RENDER_DOT, GV_RENDER_GIF, GV_RENDER_JPEG, GV_RENDER_PDF,
     GV_RENDER_PLAIN, GV_RENDER_PNG, GV_RENDER_SVG);

resourcestring

  { Library errors }

  GV_ERR_CLOSE_GRAPH = 'Error closing graph.';
  GV_ERR_CREATE_LAYOUT = 'Error performing layout.';
  GV_ERR_FREE_CONTEXT = 'Error freeing context.';
  GV_ERR_FREE_LAYOUT = 'Error freeing layout.';
  GV_ERR_PARSE_ARGS = 'Error parsing arguments.';
  GV_ERR_RENDER_GRAPH = 'Error rendering graph.';
  GV_ERR_RENDER_TO_FILE = 'Error rendering to file.';
  GV_ERR_SET_ATTRIBUTE = 'Error setting attribute value.';
  GV_ERR_UNKNOWN = 'Unknown Graphviz error.';

  { Wrapper exceptions }

  GV_ERR_DUP_ATTRIBUTE = 'Attribute "%s" already exists in this object.';
  GV_ERR_DUP_OBJECT = '%s with name "%s" already exists in this graph.';
  GV_ERR_INVALID_ATTR_NAME = '"%s" is not a valid attribute name.';
  GV_ERR_INVALID_OBJ_NAME = '"%s" is not a valid object name.';

  { Miscellaneous }

  GV_EDGE_TYPE_NAME = 'Edge';
  GV_NODE_TYPE_NAME = 'Node';

implementation

end.
