unit Graphviz.Wrapper.Points;

interface

uses
  Classes, Contnrs,
  Graphviz.Wrapper.Types;

type

  TGVPointObj = class
    private
      FPoint : TGVPoint;
    public
      constructor Create(AX, AY : TGVCoord); overload;
      constructor Create(APoint : TGVPoint); overload;

      property Point : TGVPoint read FPoint write FPoint;
      property X : TGVCoord read FPoint.X write FPoint.X;
      property Y : TGVCoord read FPoint.Y write FPoint.Y;
  end;

  TGVPointObjListEnumerator = class (TListEnumerator)
    public
      function GetCurrent : TGVPointObj;

      property Current : TGVPointObj read GetCurrent;
  end;

  TGVPointObjList = class (TObjectList)
    protected
      function  GetItem(Index : Integer) : TGVPointObj;
      procedure SetItem(Index : Integer; AObject : TGVPointObj);
    public
      constructor Create(const Points : TGVPoints); overload;

      function  Add(AObject : TGVPointObj) : Integer;
      function  Extract(Item : TGVPointObj) : TGVPointObj;
      function  First : TGVPointObj;
      procedure FromArray(const Points : TGVPoints);
      function  GetEnumerator : TGVPointObjListEnumerator;
      function  IndexOf(AObject : TGVPointObj) : Integer;
      procedure Insert(Index : Integer; AObject : TGVPointObj);
      function  Last : TGVPointObj;
      function  Remove(AObject : TGVPointObj) : Integer;
      function  ToArray : TGVPoints;

      property Items[Index : Integer] : TGVPointObj read GetItem write SetItem; default;
  end;

implementation

{ TGVPointObj }

constructor TGVPointObj.Create(APoint : TGVPoint);
begin
  inherited Create;

  FPoint := APoint;
end;

constructor TGVPointObj.Create(AX, AY : TGVCoord);
begin
  inherited Create;

  FPoint.X := AX;
  FPoint.Y := AY;
end;

{ TGVPointObjListEnumerator }

function TGVPointObjListEnumerator.GetCurrent : TGVPointObj;
begin
  Result := TGVPointObj(inherited GetCurrent);
end;

{ TGVPointObjList }

function TGVPointObjList.Add(AObject : TGVPointObj) : Integer;
begin
  Result := inherited Add(AObject);
end;

constructor TGVPointObjList.Create(const Points : TGVPoints);
begin
  inherited Create(True);

  FromArray(Points);
end;

function TGVPointObjList.Extract(Item : TGVPointObj) : TGVPointObj;
begin
  Result := inherited Extract(Item) as TGVPointObj;
end;

function TGVPointObjList.First : TGVPointObj;
begin
  Result := inherited First as TGVPointObj;
end;

procedure TGVPointObjList.FromArray(const Points : TGVPoints);
  var
    P : TGVPoint;
begin
  Clear;
  OwnsObjects := True;
  for P in Points do
    Add(TGVPointObj.Create(P));
end;

function TGVPointObjList.GetEnumerator : TGVPointObjListEnumerator;
begin
  Result := TGVPointObjListEnumerator.Create(Self);
end;

function TGVPointObjList.GetItem(Index : Integer) : TGVPointObj;
begin
  Result := inherited GetItem(Index) as TGVPointObj;
end;

function TGVPointObjList.IndexOf(AObject : TGVPointObj) : Integer;
begin
  Result := inherited IndexOf(AObject);
end;

procedure TGVPointObjList.Insert(Index : Integer; AObject : TGVPointObj);
begin
  inherited Insert(Index, AObject);
end;

function TGVPointObjList.Last : TGVPointObj;
begin
  Result := inherited Last as TGVPointObj;
end;

function TGVPointObjList.Remove(AObject : TGVPointObj) : Integer;
begin
  Result := inherited Remove(AObject);
end;

procedure TGVPointObjList.SetItem(Index : Integer; AObject : TGVPointObj);
begin
  inherited SetItem(Index, AObject);
end;

function TGVPointObjList.ToArray : TGVPoints;
  var
    i : Integer;
begin
  SetLength(Result, Count);
  for i := 0 to Count - 1 do
    Result[i] := Items[i].Point;
end;

end.
