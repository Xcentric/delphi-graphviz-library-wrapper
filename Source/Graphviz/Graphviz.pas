{
  The original repository:
  https://bitbucket.org/Xcentric/delphi-graphviz-library-wrapper
}

unit Graphviz;

interface

uses
  SysUtils,
  Graphviz.Import.Structures, Graphviz.Import.Types,
  Graphviz.Wrapper.API, Graphviz.Wrapper.GraphObjects, Graphviz.Wrapper.Nodes, Graphviz.Wrapper.Edges,
  Graphviz.Wrapper.Types;

type

  TGraphviz = class sealed (TGraphvizAPI)
    strict private
      FEdges : TGVEdgeList;
      FNodes : TGVNodeList;
    private
      procedure RenderToFiles(GVC : PGVContext; Graph : PGVGraph;
        const FileName : TFileName; FileFormats : TGVRenderFormats); inline;
    public
      constructor Create;
      destructor Destroy; override; final;

      procedure LayoutGraph(Engine : TGVLayoutEngine); overload;
      procedure LayoutGraph(Engine : TGVLayoutEngine; const FileName : TFileName; FileFormats : TGVRenderFormats); overload;

      property Edges : TGVEdgeList read FEdges;
      property Nodes : TGVNodeList read FNodes;
  end;

implementation

uses
  Graphviz.Import.Intf, Graphviz.Import.Consts,
  Graphviz.Wrapper.Attributes, Graphviz.Wrapper.Consts, Graphviz.Wrapper.Exceptions;

{ TGraphviz }

constructor TGraphviz.Create;
begin
  inherited Create;

  FNodes := TGVNodeList.Create;
  FEdges := TGVEdgeList.Create;
end;

destructor TGraphviz.Destroy;
begin
  FEdges.Free;
  FNodes.Free;

  inherited Destroy;
end;

procedure TGraphviz.LayoutGraph(Engine : TGVLayoutEngine);
begin
  LayoutGraph(Engine, EmptyStr, []);
end;

procedure TGraphviz.LayoutGraph(Engine : TGVLayoutEngine; const FileName : TFileName; FileFormats : TGVRenderFormats);

  type
    TGVObjRec = record
      Obj : TGVObject;
      Attrs : TGVAttributes;
    end;
    TGVObjRecs = array of TGVObjRec;

  procedure InitGVObjArray(GVObjList : TGVObjectList; out GVObjArray : TGVObjRecs);
    var
      Obj : TGVObject;
      iOldLength : Integer;
  begin
    for Obj in GVObjList do
    begin
      Obj.PutDimensionsToAttributes;
      iOldLength := Length(GVObjArray);
      SetLength(GVObjArray, iOldLength + 1);
      GVObjArray[iOldLength].Obj := Obj;
      GVObjArray[iOldLength].Attrs := Obj.Attributes.ToArray;
    end;
  end;

  procedure UpdateDimensions(GVObjList : TGVObjectList);
    var
      Obj : TGVObject;
  begin
    for Obj in GVObjList do
      Obj.SetDimensionsFromAttributes;
  end;

  var
    arrNodes, arrEdges : TGVObjRecs;
    ObjRec :  TGVObjRec;
    Attr : TGVAttribute;
    Ctx : PGVContext;
    G : PGVGraph;
    N, A, B : PGVNode;
    E : PGVEdge;
begin
  InitGVObjArray(FNodes, arrNodes);
  InitGVObjArray(FEdges, arrEdges);

  { INLINED CALLS ONLY ZONE - BEGIN }
  
  Ctx := CreateContext;
  G := OpenGraph(ClassName, @TGVGraphDesc.CreateDefault);

  for ObjRec in arrNodes do
  begin
    N := GetNode(G, ObjRec.Obj.Name, True);

    if Assigned(N) then
      for Attr in ObjRec.Attrs do
        SetAttribValue(N, Attr.Name, Attr.Value);
  end;

  for ObjRec in arrEdges do
  begin
    A := GetNode(G, TGVEdge(ObjRec.Obj).NodeA.Name, False);
    B := GetNode(G, TGVEdge(ObjRec.Obj).NodeB.Name, False);

    if Assigned(A) and Assigned(B) then
    begin
      E := GetEdge(G, A, B, ObjRec.Obj.Name, True);

      if Assigned(E) then
        for Attr in ObjRec.Attrs do
          SetAttribValue(E, Attr.Name, Attr.Value);
    end;
  end;

  PerformLayout(Ctx, G, LayoutEngineNames[Engine]);
  RenderGraph(Ctx, G, GV_RENDER_DOT);
  RenderToFiles(Ctx, G, FileName, FileFormats);
  FreeLayout(Ctx, G);

  for ObjRec in arrNodes do
  begin
    N := GetNode(G, ObjRec.Obj.Name, False);

    if Assigned(N) then
      for Attr in ObjRec.Attrs do
        Attr.Value := GetAttribValue(N, Attr.Name);
  end;

  for ObjRec in arrEdges do
  begin
    A := GetNode(G, TGVEdge(ObjRec.Obj).NodeA.Name, False);
    B := GetNode(G, TGVEdge(ObjRec.Obj).NodeB.Name, False);

    if Assigned(A) and Assigned(B) then
    begin
      E := GetEdge(G, A, B, ObjRec.Obj.Name, False);

      if Assigned(E) then
        for Attr in ObjRec.Attrs do
          Attr.Value := GetAttribValue(E, Attr.Name);
    end;
  end;

  CloseGraph(G);
  FreeContext(Ctx);

  { INLINED CALLS ONLY ZONE - END }

  UpdateDimensions(FNodes);
  UpdateDimensions(FEdges);
end;

procedure TGraphviz.RenderToFiles(GVC : PGVContext; Graph : PGVGraph; const FileName : TFileName;
  FileFormats : TGVRenderFormats);
  var
    RF : TGVRenderFormat;
begin
  if FileName <> GV_EMPTY_STR then
    for RF in FileFormats do
      RenderGraphToFile(GVC, Graph, RenderFormatNames[RF],
        Format('%s.%s', [FileName, LowerCase(RenderFormatNames[RF])]));
end;

end.
