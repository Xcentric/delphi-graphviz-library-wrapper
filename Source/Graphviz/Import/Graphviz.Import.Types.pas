unit Graphviz.Import.Types;

interface

type

  PGVPointer = type Pointer;

  PGVContext = type PGVPointer;
  PGVString = type PAnsiChar;
  PGVStringArray = array of PGVString;
  TGVBool = type LongBool;

implementation

end.
