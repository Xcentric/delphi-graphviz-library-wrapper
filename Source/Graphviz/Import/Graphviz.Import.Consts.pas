unit Graphviz.Import.Consts;

interface

// DO NOT LOCALIZE !!!

const

  { Libraries }

  CGRAPH_LIB = 'cgraph.dll';
  GVC_LIB = 'gvc.dll';

  { Graph types }

  AGFLAG_DIRECTED  = 1 shl 0;
  AGFLAG_STRICT = 1 shl 1;
  AGFLAG_METAGRAPH = 1 shl 2;

  AGRAPH = 0;
  AGRAPHSTRICT = AGRAPH or AGFLAG_STRICT;
  AGDIGRAPH = AGFLAG_DIRECTED;
  AGDIGRAPHSTRICT = AGDIGRAPH or AGFLAG_STRICT;
  AGMETAGRAPH = AGFLAG_DIRECTED or AGFLAG_STRICT or AGFLAG_METAGRAPH;

  { Layout engines }

  GV_ENGINE_CIRCO = 'circo';
  GV_ENGINE_DOT = 'dot';
  GV_ENGINE_FDP = 'fdp';
  GV_ENGINE_NEATO = 'neato';
  GV_ENGINE_OSAGE = 'osage';
  GV_ENGINE_SFDP = 'sfdp';
  GV_ENGINE_TWOPI = 'twopi';

  { Render formats }

  GV_RENDER_BMP = 'bmp';
  GV_RENDER_CANON = 'canon';
  GV_RENDER_CGIMAGE = 'cgimage';
  GV_RENDER_CMAP = 'cmap';
  GV_RENDER_CMAPX = 'cmapx';
  GV_RENDER_CMAPXNP = 'cmapx_np';
  GV_RENDER_DOT = 'dot';
  GV_RENDER_EPS = 'eps';
  GV_RENDER_EXR = 'exr';
  GV_RENDER_FIG = 'fig';
  GV_RENDER_GD = 'gd';
  GV_RENDER_GD2 = 'gd2';
  GV_RENDER_GIF = 'gif';
  GV_RENDER_GTK = 'gtk';
  GV_RENDER_GV = 'gv';
  GV_RENDER_ICO = 'ico';
  GV_RENDER_IMAP = 'imap';
  GV_RENDER_IMAPNP = 'imap_np';
  GV_RENDER_ISMAP = 'ismap';
  GV_RENDER_JP2 = 'jp2';
  GV_RENDER_JPE = 'jpe';
  GV_RENDER_JPEG = 'jpeg';
  GV_RENDER_JPG = 'jpg';
  GV_RENDER_PCT = 'pct';
  GV_RENDER_PDF = 'pdf';
  GV_RENDER_PIC = 'pic';
  GV_RENDER_PICT = 'pict';
  GV_RENDER_PLAIN = 'plain';
  GV_RENDER_PLAINEXT = 'plain-ext';
  GV_RENDER_PNG = 'png';
  GV_RENDER_POV = 'pov';
  GV_RENDER_PS = 'ps';
  GV_RENDER_PS2 = 'ps2';
  GV_RENDER_PSD = 'psd';
  GV_RENDER_SGI = 'sgi';
  GV_RENDER_SVG = 'svg';
  GV_RENDER_SVGZ = 'svgz';
  GV_RENDER_TGA = 'tga';
  GV_RENDER_TIF = 'tif';
  GV_RENDER_TIFF = 'tiff';
  GV_RENDER_TK = 'tk';
  GV_RENDER_VML = 'vml';
  GV_RENDER_VMLZ = 'vmlz';
  GV_RENDER_VRML = 'vrml';
  GV_RENDER_WBMP = 'wbmp';
  GV_RENDER_WEBP = 'webp';
  GV_RENDER_X11 = 'x11';
  GV_RENDER_XDOT = 'xdot';
  GV_RENDER_XDOT12 = 'xdot1.2';
  GV_RENDER_XDOT14 = 'xdot1.4';
  GV_RENDER_XLIB = 'xlib';

  { Attributes }

  GV_ATTR_BACKGROUND = '_background';
  GV_ATTR_AREA = 'area';
  GV_ATTR_ARROWHEAD = 'arrowhead';
  GV_ATTR_ARROWSIZE = 'arrowsize';
  GV_ATTR_ARROWTAIL = 'arrowtail';
  GV_ATTR_BB = 'bb';
  GV_ATTR_BGCOLOR = 'bgcolor';
  GV_ATTR_CENTER = 'center';
  GV_ATTR_CHARSET = 'charset';
  GV_ATTR_CLUSTERRANK = 'clusterrank';
  GV_ATTR_COLOR = 'color';
  GV_ATTR_COLORSCHEME = 'colorscheme';
  GV_ATTR_COMMENT = 'comment';
  GV_ATTR_COMPOUND = 'compound';
  GV_ATTR_CONCENTRATE = 'concentrate';
  GV_ATTR_CONSTRAINT = 'constraint';
  GV_ATTR_DAMPING = 'Damping';
  GV_ATTR_DECORATE = 'decorate';
  GV_ATTR_DEFAULTDIST = 'defaultdist';
  GV_ATTR_DIM = 'dim';
  GV_ATTR_DIMEN = 'dimen';
  GV_ATTR_DIR = 'dir';
  GV_ATTR_DIREDGECONSTRAINTS = 'diredgeconstraints';
  GV_ATTR_DISTORTION = 'distortion';
  GV_ATTR_DPI = 'dpi';
  GV_ATTR_EDGEHREF = 'edgehref';
  GV_ATTR_EDGETARGET = 'edgetarget';
  GV_ATTR_EDGETOOLTIP = 'edgetooltip';
  GV_ATTR_EDGEURL = 'edgeURL';
  GV_ATTR_EPSILON = 'epsilon';
  GV_ATTR_ESEP = 'esep';
  GV_ATTR_FILLCOLOR = 'fillcolor';
  GV_ATTR_FIXEDSIZE = 'fixedsize';
  GV_ATTR_FONTCOLOR = 'fontcolor';
  GV_ATTR_FONTNAME = 'fontname';
  GV_ATTR_FONTNAMES = 'fontnames';
  GV_ATTR_FONTPATH = 'fontpath';
  GV_ATTR_FONTSIZE = 'fontsize';
  GV_ATTR_FORCELABELS = 'forcelabels';
  GV_ATTR_GRADIENTANGLE = 'gradientangle';
  GV_ATTR_GROUP = 'group';
  GV_ATTR_HEADLP = 'head_lp';
  GV_ATTR_HEADCLIP = 'headclip';
  GV_ATTR_HEADHREF = 'headhref';
  GV_ATTR_HEADLABEL = 'headlabel';
  GV_ATTR_HEADPORT = 'headport';
  GV_ATTR_HEADTARGET = 'headtarget';
  GV_ATTR_HEADTOOLTIP = 'headtooltip';
  GV_ATTR_HEADURL = 'headURL';
  GV_ATTR_HEIGHT = 'height';
  GV_ATTR_HREF = 'href';
  GV_ATTR_ID = 'id';
  GV_ATTR_IMAGE = 'image';
  GV_ATTR_IMAGEPATH = 'imagepath';
  GV_ATTR_IMAGESCALE = 'imagescale';
  GV_ATTR_INPUTSCALE = 'inputscale';
  GV_ATTR_K = 'K';
  GV_ATTR_LABEL = 'label';
  GV_ATTR_LABELSCHEME = 'label_scheme';
  GV_ATTR_LABELANGLE = 'labelangle';
  GV_ATTR_LABELDISTANCE = 'labeldistance';
  GV_ATTR_LABELFLOAT = 'labelfloat';
  GV_ATTR_LABELFONTCOLOR = 'labelfontcolor';
  GV_ATTR_LABELFONTNAME = 'labelfontname';
  GV_ATTR_LABELFONTSIZE = 'labelfontsize';
  GV_ATTR_LABELHREF = 'labelhref';
  GV_ATTR_LABELJUST = 'labeljust';
  GV_ATTR_LABELLOC = 'labelloc';
  GV_ATTR_LABELTARGET = 'labeltarget';
  GV_ATTR_LABELTOOLTIP = 'labeltooltip';
  GV_ATTR_LABELURL = 'labelURL';
  GV_ATTR_LANDSCAPE = 'landscape';
  GV_ATTR_LAYER = 'layer';
  GV_ATTR_LAYERLISTSEP = 'layerlistsep';
  GV_ATTR_LAYERS = 'layers';
  GV_ATTR_LAYERSELECT = 'layerselect';
  GV_ATTR_LAYERSEP = 'layersep';
  GV_ATTR_LAYOUT = 'layout';
  GV_ATTR_LEN = 'len';
  GV_ATTR_LEVELS = 'levels';
  GV_ATTR_LEVELSGAP = 'levelsgap';
  GV_ATTR_LHEAD = 'lhead';
  GV_ATTR_LHEIGHT = 'lheight';
  GV_ATTR_LP = 'lp';
  GV_ATTR_LTAIL = 'ltail';
  GV_ATTR_LWIDTH = 'lwidth';
  GV_ATTR_MARGIN = 'margin';
  GV_ATTR_MAXITER = 'maxiter';
  GV_ATTR_MCLIMIT = 'mclimit';
  GV_ATTR_MINDIST = 'mindist';
  GV_ATTR_MINLEN = 'minlen';
  GV_ATTR_MODE = 'mode';
  GV_ATTR_MODEL = 'model';
  GV_ATTR_MOSEK = 'mosek';
  GV_ATTR_NODESEP = 'nodesep';
  GV_ATTR_NOJUSTIFY = 'nojustify';
  GV_ATTR_NORMALIZE = 'normalize';
  GV_ATTR_NOTRANSLATE = 'notranslate';
  GV_ATTR_NSLIMIT = 'nslimit';
  GV_ATTR_NSLIMIT1 = 'nslimit1';
  GV_ATTR_ORDERING = 'ordering';
  GV_ATTR_ORIENTATION = 'orientation';
  GV_ATTR_OUTPUTORDER = 'outputorder';
  GV_ATTR_OVERLAP = 'overlap';
  GV_ATTR_OVERLAPSCALING = 'overlap_scaling';
  GV_ATTR_OVERLAPSHRINK = 'overlap_shrink';
  GV_ATTR_PACK = 'pack';
  GV_ATTR_PACKMODE = 'packmode';
  GV_ATTR_PAD = 'pad';
  GV_ATTR_PAGE = 'page';
  GV_ATTR_PAGEDIR = 'pagedir';
  GV_ATTR_PENCOLOR = 'pencolor';
  GV_ATTR_PENWIDTH = 'penwidth';
  GV_ATTR_PERIPHERIES = 'peripheries';
  GV_ATTR_PIN = 'pin';
  GV_ATTR_POS = 'pos';
  GV_ATTR_QUADTREE = 'quadtree';
  GV_ATTR_QUANTUM = 'quantum';
  GV_ATTR_RANK = 'rank';
  GV_ATTR_RANKDIR = 'rankdir';
  GV_ATTR_RANKSEP = 'ranksep';
  GV_ATTR_RATIO = 'ratio';
  GV_ATTR_RECTS = 'rects';
  GV_ATTR_REGULAR = 'regular';
  GV_ATTR_REMINCROSS = 'remincross';
  GV_ATTR_REPULSIVEFORCE = 'repulsiveforce';
  GV_ATTR_RESOLUTION = 'resolution';
  GV_ATTR_ROOT = 'root';
  GV_ATTR_ROTATE = 'rotate';
  GV_ATTR_ROTATION = 'rotation';
  GV_ATTR_SAMEHEAD = 'samehead';
  GV_ATTR_SAMETAIL = 'sametail';
  GV_ATTR_SAMPLEPOINTS = 'samplepoints';
  GV_ATTR_SCALE = 'scale';
  GV_ATTR_SEARCHSIZE = 'searchsize';
  GV_ATTR_SEP = 'sep';
  GV_ATTR_SHAPE = 'shape';
  GV_ATTR_SHAPEFILE = 'shapefile';
  GV_ATTR_SHOWBOXES = 'showboxes';
  GV_ATTR_SIDES = 'sides';
  GV_ATTR_SIZE = 'size';
  GV_ATTR_SKEW = 'skew';
  GV_ATTR_SMOOTHING = 'smoothing';
  GV_ATTR_SORTV = 'sortv';
  GV_ATTR_SPLINES = 'splines';
  GV_ATTR_START = 'start';
  GV_ATTR_STYLE = 'style';
  GV_ATTR_STYLESHEET = 'stylesheet';
  GV_ATTR_TAILLP = 'tail_lp';
  GV_ATTR_TAILCLIP = 'tailclip';
  GV_ATTR_TAILHREF = 'tailhref';
  GV_ATTR_TAILLABEL = 'taillabel';
  GV_ATTR_TAILPORT = 'tailport';
  GV_ATTR_TAILTARGET = 'tailtarget';
  GV_ATTR_TAILTOOLTIP = 'tailtooltip';
  GV_ATTR_TAILURL = 'tailURL';
  GV_ATTR_TARGET = 'target';
  GV_ATTR_TOOLTIP = 'tooltip';
  GV_ATTR_TRUECOLOR = 'truecolor';
  GV_ATTR_URL = 'URL';
  GV_ATTR_VERTICES = 'vertices';
  GV_ATTR_VIEWPORT = 'viewport';
  GV_ATTR_VOROMARGIN = 'voro_margin';
  GV_ATTR_WEIGHT = 'weight';
  GV_ATTR_WIDTH = 'width';
  GV_ATTR_XDOTVERSION = 'xdotversion';
  GV_ATTR_XLABEL = 'xlabel';
  GV_ATTR_XLP = 'xlp';
  GV_ATTR_Z = 'z';

implementation

end.
