unit Graphviz.Import.Intf;

interface

uses
  Graphviz.Import.Structures, Graphviz.Import.Types, Graphviz.Import.Consts;

  { cgraph }

  function agclose(Graph : PGVGraph) : Integer; stdcall; external CGRAPH_LIB;
  function agedge(Graph : PGVGraph; NodeA, NodeB : PGVNode; EdgeName : PGVString; CreateFlag : TGVBool) : PGVEdge; stdcall; external CGRAPH_LIB;
  function agfstedge(Graph : PGVGraph; OrgNode : PGVNode) : PGVEdge; stdcall; external CGRAPH_LIB;
  function agfstnode(Graph : PGVGraph) : PGVNode; stdcall; external CGRAPH_LIB;
  function agget(Obj : PGVPointer; AttribName : PGVString) : PGVString; stdcall; external CGRAPH_LIB;
  function agnode(Graph : PGVGraph; NodeName : PGVString; CreateFlag : TGVBool) : PGVNode; stdcall; external CGRAPH_LIB;
  function agnxtedge(Graph : PGVGraph; PrevEdge : PGVEdge; OrgNode : PGVNode) : PGVEdge; stdcall; external CGRAPH_LIB;
  function agnxtnode(Graph : PGVGraph; PrevNode : PGVNode) : PGVNode; stdcall; external CGRAPH_LIB;
  function agopen(GraphName : PGVString; GraphType : PGVGraphDesc; Overrides : PGVPointer = nil) : PGVGraph; stdcall; external CGRAPH_LIB;
  function agsafeset(Obj : PGVPointer; AttribName, AttribValue, AttribDefValue : PGVString) : Integer; stdcall; external CGRAPH_LIB;

  { gvc }

  function gvContext : PGVContext; stdcall; external GVC_LIB;
  function gvFreeContext(GVC : PGVContext) : Integer; stdcall; external GVC_LIB;
  function gvFreeLayout(GVC : PGVContext; Graph : PGVGraph) : Integer; stdcall; external GVC_LIB;
  function gvLayout(GVC : PGVContext; Graph : PGVGraph; EngineName : PGVString) : Integer; stdcall; external GVC_LIB;
  function gvParseArgs(GVC : PGVContext; ArgsCount : Integer; ArgsArray : PGVStringArray) : Integer; stdcall; external GVC_LIB;
  function gvRender(GVC : PGVContext; Graph : PGVGraph; EngineName : PGVString; StdOutput : PGVPointer = nil) : Integer; stdcall; external GVC_LIB;
  function gvRenderFilename(GVC : PGVContext; Graph : PGVGraph; FileFormat, FileName : PGVString) : Integer; stdcall; external GVC_LIB;

implementation

end.
