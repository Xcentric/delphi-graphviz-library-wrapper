unit Graphviz.Import.Structures;

interface

uses
  Graphviz.Import.Types;

type

  { Declarations }

  Agdesc_s = packed record
    IsDirected,
    IsStrict,
    NoLoopsAllowed,
    IsMainGraph,
    FlatLockEnabled,
    NoWriteAllowed,
    HasAttributes,
    HasCompound : TGVBool;

    constructor CreateDefault;
  end;
  PAgdesc_s = ^Agdesc_s;

  PAgedge_t = type PGVPointer;
  PAgnode_t = type PGVPointer;
  PAgraph_t = type PGVPointer;
  PAgrec_t  = type PGVPointer;
  PAgsym_t  = type PGVPointer;

  { Aliases }

  PGVAttribute = PAgsym_t;
  PGVCustomData = PAgrec_t;
  PGVEdge = PAgedge_t;
  PGVGraph = PAgraph_t;
  PGVGraphDesc = PAgdesc_s;
  PGVNode = PAgnode_t;
  TGVGraphDesc = Agdesc_s;

implementation

{ Agdesc_s }

constructor Agdesc_s.CreateDefault;
begin
  IsDirected      := True;
  IsStrict        := True;
  NoLoopsAllowed  := True;
  IsMainGraph     := True;
  FlatLockEnabled := True;
  NoWriteAllowed  := False;
  HasAttributes   := True;
  HasCompound     := True;
end;

end.
