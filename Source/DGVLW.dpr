program DGVLW;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  Main in 'Main.pas',
  Graphviz.Import.Consts in 'Graphviz\Import\Graphviz.Import.Consts.pas',
  Graphviz.Import.Intf in 'Graphviz\Import\Graphviz.Import.Intf.pas',
  Graphviz.Import.Structures in 'Graphviz\Import\Graphviz.Import.Structures.pas',
  Graphviz.Import.Types in 'Graphviz\Import\Graphviz.Import.Types.pas',
  Graphviz.Wrapper.API in 'Graphviz\Wrapper\Graphviz.Wrapper.API.pas',
  Graphviz.Wrapper.Exceptions in 'Graphviz\Wrapper\Graphviz.Wrapper.Exceptions.pas',
  Graphviz.Wrapper.Consts in 'Graphviz\Wrapper\Graphviz.Wrapper.Consts.pas',
  Graphviz.Wrapper.Utils in 'Graphviz\Wrapper\Graphviz.Wrapper.Utils.pas',
  Graphviz.Wrapper.Types in 'Graphviz\Wrapper\Graphviz.Wrapper.Types.pas',
  Graphviz.Wrapper.Nodes in 'Graphviz\Wrapper\Graphviz.Wrapper.Nodes.pas',
  Graphviz.Wrapper.Edges in 'Graphviz\Wrapper\Graphviz.Wrapper.Edges.pas',
  Graphviz.Wrapper.GraphObjects in 'Graphviz\Wrapper\Graphviz.Wrapper.GraphObjects.pas',
  Graphviz.Wrapper.Attributes in 'Graphviz\Wrapper\Graphviz.Wrapper.Attributes.pas',
  Graphviz.Wrapper.Points in 'Graphviz\Wrapper\Graphviz.Wrapper.Points.pas',
  Graphviz in 'Graphviz\Graphviz.pas';

begin
  try
    MainProc;
    WriteLn('>>> Press Enter to exit...');
    ReadLn;
  except
    on E:Exception do
    begin
      WriteLn(E.Classname, ': ', E.Message);
      Readln;
    end;
  end;
end.
